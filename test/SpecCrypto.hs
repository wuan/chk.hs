{-# LANGUAGE OverloadedStrings #-}

module SpecCrypto
  ( tests
  ) where

import qualified Data.ByteString as B

import Data.Char
 ( ord
 )

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  , encodeBase32
  )
import Test.Tasty
  ( TestTree
  , testGroup
  )

import Test.Tasty.HUnit
  ( testCase
  , assertEqual
  )

import Crypto
  ( sha256
  , sha256d
  , storageIndexHash
  , convergenceEncryptionTag
  )

import Types
  ( Parameters(..)
  )

import Crypto.Cipher.AES128
  ( AESKey128
  )

import Crypto.Classes
  ( buildKey
  )

tests :: TestTree
tests = testGroup "Crypto"
  [ testGroup "sha256"
    [ testCase "sha256" $
      assertEqual "known value"
      "\x23\xfb\xe2\x1e\x2f\xae\xde\xb5\x44\x92\xcb\x7a\xc6\x0e\x04\x4a\xbb\x47\x3f\xcb\x13\x1a\x65\x8e\xd2\x5c\xd0\x17\x06\xc3\xf3\x98"
      (sha256 "4:tag1,value")

    , testCase "sha256d" $
      assertEqual "known value"
      "\x6b\xd0\xdc\xb0\x2f\x11\x0a\xe1\xe9\x41\x1f\x12\x52\x07\x03\x66\xfe\xaa\xcb\xc9\xda\xdb\x66\xa4\xa9\xa0\xc0\xdd\x85\x49\x5d\xc4"
      (sha256d "4:tag1,value")

    , testCase "storage index tagged hash" $
      -- Adapted from allmydata.test.test_hashutil.HashUtilTests.test_known_answers
      assertEqual "known value"
        (Just "\xb5\x4c\x60\xc5\xb1\x26\x46\xf0\x77\x0\xc4\x4c\x8b\x75\xb9\x48")
        (storageIndexHash <$> xKey)
    , testCase "convergence hasher tag" $
      -- See allmydata.test.test_hashutil.HashUtilTests.test_convergence_hasher_tag
      let
        convergenceSecret = B.replicate 16 0x42
        params = Parameters
                 { paramSegmentSize = 1024
                 , paramTotalShares = 10
                 , paramHappyShares = undefined
                 , paramRequiredShares = 3
                 }
      in
        assertEqual "known value"
          (mconcat
           [ "allmydata_immutable_content_to_key_with_added_secret_v1+"
           , "16:\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42\x42,"
           , "9:3,10,1024,"
           ])
          (convergenceEncryptionTag convergenceSecret params)

    ]
  ]
  where
    xKey = buildKey (B.replicate 16 . fromIntegral . ord $ 'x') :: Maybe AESKey128
