{-# LANGUAGE OverloadedStrings #-}

module SpecMerkle
  ( tests
  ) where

import Data.Maybe
  ( isJust
  )

import Data.Text
  ( pack
  )

import Data.Text.Encoding
  ( encodeUtf8
  )

import Data.List
  ( sort
  )
import Test.Tasty
  ( TestTree
  , testGroup
  )

import Test.Tasty.HUnit
  ( testCase
  , assertEqual
  , assertBool
  )

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  )

import Crypto
  ( taggedHash
  )

import Merkle
  ( MerkleTree(MerkleNode, MerkleLeaf)
  , makeTree
  , breadthFirstList
  , pairHash
  , emptyLeafHash
  , rootHash
  , neededHashes
  )

tests :: TestTree
tests = testGroup "Merkle"
  [ testCase "pairHash" $
    assertEqual "simple test vector"
    "MNP3F5B64GHVUPQ3U7ZT76D7ZP6NVHHV5KMFLT2IPORIGI5EL57Q"
    (encodeBase32Unpadded $ pairHash "abc" "xyz")
  , testCase "emptyLeafHash" $
    assertEqual "simple test vector"
    "T3KZA5VWX3TLOWDEMMGDYIGP62JU57QDUYFH7UULNFKC7MJ2NCRQ"
    (encodeBase32Unpadded $ emptyLeafHash 3)

  , testCase "two leaf tree" $
    assertEqual "root hash is leaf pair hash"
    (Just "MNP3F5B64GHVUPQ3U7ZT76D7ZP6NVHHV5KMFLT2IPORIGI5EL57Q")
    (encodeBase32Unpadded . rootHash <$> makeTree ["abc", "xyz"])

  , testCase "three leaf tree" $
    assertEqual "root hash of three leaf tree includes empty node hash"
    (Just $ encodeBase32Unpadded $ pairHash (pairHash "abc" "xyz") (pairHash "mno" $ emptyLeafHash 3))
    (encodeBase32Unpadded . rootHash <$> makeTree ["abc", "xyz", "mno"])

  , testCase "empty tree" $
    assertEqual "empty list results in no tree"
    Nothing
    (makeTree [])
  , testCase "tiny tree" $
    assertEqual "a two leaf tree can be constructed"
    (Just (MerkleNode "\138\134\149S\148\&6\239\SO\217\208Si\153\SUB\176oHEb\169(\233C\150\155\176g<\242N\DEL=" (MerkleLeaf "bar") (MerkleLeaf "baz")))
    (makeTree ["bar", "baz"])
  , testCase "make 6 leaf tree" $
    assertBool "it can be made" $
    isJust (makeTestTree 6)
  , testCase "breadth first traversal (small)" $
    assertEqual "tree with one leaf"
    (Just 1)
    (length . breadthFirstList <$> makeTestTree 1)
  , testCase "breadth first traversal (big)" $
    assertEqual "tree with 1024 leaves"
    (Just (1024 * 2 - 1))
    (length . breadthFirstList <$> makeTestTree 1024)
  , testCase "show it" $ do
    print $ makeTestTree 2
    return ()
  , testCase "needed_hashes test vectors" $
    let
      Just tree = makeTestTree 8
      needed = sort . map fst . neededHashes tree
    in do
      assertEqual "test vector 1" [2 :: Int, 4, 8] (needed 0)
      assertEqual "test vector 2" [2, 4, 7] (needed 1)
      assertEqual "test vector 3" [1, 5, 13] (needed 7)
  ]
  where
    makeTestTree numleaves =
      makeTree [ taggedHash 32 "tag" (encodeUtf8 . pack . show $ n) | n <- [0..numleaves-1] ]
