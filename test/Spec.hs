module Main where

import Test.Tasty
  ( TestTree
  , testGroup
  , defaultMain
  )

import qualified SpecUpload
import qualified SpecCrypto
import qualified SpecCHK
import qualified SpecMerkle

tests :: TestTree
tests = testGroup "All"
  [ SpecUpload.tests
  , SpecCrypto.tests
  , SpecCHK.tests
  , SpecMerkle.tests
  ]

main = defaultMain tests
