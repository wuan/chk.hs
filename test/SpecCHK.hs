{-# LANGUAGE OverloadedStrings #-}

module SpecCHK
  ( tests
  ) where

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL

import Data.Char
 ( ord
 )

import Data.Text
  ( Text
  )

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  , encodeBase32
  )
import Data.ByteString.Base64
  ( encodeBase64
  )

import Test.Tasty
  ( TestTree
  , testGroup
  )

import Test.Tasty.HUnit
  ( testCase
  , assertEqual
  )

import Types
  ( Parameters(..)
  )

import Upload
  ( Capability(capabilityText)
  , getConvergentKey
  , adjustSegmentSize
  )

import CHK
  ( chkCap
  , chkEncrypt
  )

import URIExtension
  ( URIExtension(..)
  )

import Crypto
  ( sha256
  , sha256d
  )

import Crypto.Cipher.AES128
  ( AESKey128
  )

import Crypto.Classes
  ( buildKey
  , encode
  )

tests :: TestTree
tests = testGroup "CHK"
  [ testCap
  , testEncrypt
  ]

testEncrypt :: TestTree
testEncrypt = testGroup "chkEncrypt"
  [ testCase "ciphertext" $ do
      assertEqual "expected convergence key"
        "oBcuR/wKdCgCV2GKKXqiNg=="
        (encodeBase64 $ encode convergenceKey)
      ciphertext <- encrypt
      let b64ciphertext = encodeBase64 ciphertext
      assertEqual "known result" knownCorrect b64ciphertext
  ]
  where
    -- For all the magic values see
    -- allmydata.test.test_upload.FileHandleTests.test_get_encryption_key_convergent
    knownCorrect :: Text
    knownCorrect = "Jd2LHCRXozwrEJc="

    plaintext:: B.ByteString
    plaintext = "hello world"

    convergenceKey :: AESKey128
    convergenceKey = getConvergentKey convergenceSecret params (BL.fromStrict plaintext)

    convergenceSecret = B.replicate 16 0x42
    params = adjustSegmentSize
      Parameters
      { paramSegmentSize = 128 * 1024
      , paramTotalShares = 10
      , paramHappyShares = 5
      , paramRequiredShares = 3
      } (fromIntegral $ B.length plaintext)

    encrypt :: IO B.ByteString
    encrypt = do
      read <- chkEncrypt convergenceKey (\n -> return plaintext)
      read 1024

testCap :: TestTree
testCap = testGroup "chkCap"
  [ testCase "chkCap" $
    assertEqual "cap text"
    -- From allmydata.test.test_uri.Dirnode.test_immutable
    (Just "URI:CHK:aeaqcaibaeaqcaibaeaqcaibae:nf3nimquen7aeqm36ekgxomalstenpkvsdmf6fplj7swdatbv5oa:3:10:1234")
    (capabilityText . chkCap total required size uriExt <$> key)
  ]
  where
    key = buildKey (B.replicate 16 0x01) :: Maybe AESKey128
    total = 10
    required = 3
    size = 1234

    uriExt = URIExtension
      { uriExtCodecName = "crs"
      , uriExtCodecParams = Parameters 1024 total undefined required
        -- XXX The tail segment size should probably be different.  Doesn't
        -- matter for the single test that exists now but it might later.
        --
        -- XXX Okay it might matter because the segmentSize here is wrong.  It
        -- should be the smallest multiple of `required` that is greater than
        -- numSegments * segmentSize in codec params.  Perhaps this code
        -- computes the correct hash for the bogus values given here but
        -- Tahoe-LAFS won't ever use a URI extension with these values so it
        -- will never try to compute this hash.
      , uriExtTailCodecParams = Parameters 1024 total undefined required
      , uriExtSize = size
      , uriExtSegmentSize = 1024
      , uriExtNumSegments = 2
      , uriExtNeededShares = required
      , uriExtTotalShares = total
      , uriExtCrypttextHash = B.replicate 32 0x02
      , uriExtCrypttextRootHash = B.replicate 32 0x03
      , uriExtShareRootHash = B.replicate 32 0x04
      }
