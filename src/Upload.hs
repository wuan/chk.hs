module Upload
  ( UploadResult(uploadResultReadCap, uploadResultExistingShares, uploadResultShareMap)
  , Capability(capabilityText)
  , Parameters(Parameters)
  , defaultParameters
  , filesystemUploadable
  , filesystemUploadableWithConvergence
  , filesystemUploadableRandomConvergence
  , filesystemStorageServer
  , getConvergentKey
  , upload
  , store

  , prettyFormatSharemap
  , adjustSegmentSize
  ) where

import Control.Concurrent.MVar
  ( MVar
  , newEmptyMVar
  , putMVar
  , takeMVar
  )

import Control.Concurrent.Async
  ( async
  )

import Control.Monad
  ( void
  , zipWithM
  , guard
  , (<=<)
  )

import Control.Monad.Loops
  ( untilJust
  )

import Control.Exception
  ( catch
  )

import Data.Maybe
  ( fromJust
  )

import Data.List
 ( intersperse
 )

import Data.IORef
  ( IORef
  , newIORef
  , readIORef
  , writeIORef
  )

import Data.ByteString.Base32
  ( encodeBase32Unpadded
  )

import qualified Data.ByteString as B
import Data.ByteString
  ( ByteString
  , hPut
  )
import qualified Data.ByteString.Lazy as BL

import qualified Data.ByteArray as BA

import Data.Serialize
  ( Serialize
  , encode
  )

import qualified Data.Text as Text
import Data.Text
  ( Text
  , pack
  , unpack
  , intercalate
  )

import qualified Data.Text.IO as Text

import qualified Data.Set as Set

import qualified Data.Map.Strict as Map

import Crypto.Classes
  ( buildKeyIO
  , buildKey
  )

import Crypto.Cipher.AES128
  ( AESKey128
  )

import Crypto.Hash
  ( Digest
  , hashlazy
  )

import Crypto.Hash.Algorithms
  ( SHA256
  )

import System.FilePath.Posix
  ( (</>)
  )

import System.IO.Error
  ( isDoesNotExistError
  )

import System.IO
  ( SeekMode(AbsoluteSeek)
  , IOMode(ReadMode, ReadWriteMode)
  , Handle
  , withBinaryFile
  , openBinaryFile
  , openFile
  , hSetBinaryMode
  , readFile
  , hFileSize
  , hSeek
  )

import System.Directory
  ( createDirectoryIfMissing
  , listDirectory
  )

import Crypto
  ( storageIndexHash
  , convergenceEncryptionHashLazy
  , sha256
  )

import Types

import Util
  ( nextMultiple
  )

import CHK
  ( makeChkEncoder
  , chkEncrypt
  )

-- Find shares that already exist on servers.
locateAllShareholders :: StorageIndex -> [StorageServer] -> IO ShareMap
locateAllShareholders storageIndex servers =
  Map.unionsWith Set.union <$> mapM getBuckets servers
  where
    getBuckets :: StorageServer -> IO ShareMap
    getBuckets s = do
      buckets <- storageServerGetBuckets s storageIndex
      return $ Map.fromSet (const $ Set.singleton s) buckets

planSharePlacement :: Parameters -> StorageIndex -> ShareMap -> [StorageServer] -> ShareMap
planSharePlacement (Parameters _ total _ _) storageIndex currentShares servers =
  Map.fromList
  [ (shareNum, Set.singleton server)
  | (shareNum, server) <- zip [0..total - 1] (cycle servers)
  ]

-- Upload some immutable share data to some buckets on some servers.
--
-- XXX TODO This writes the raw share data to a file with no server-side
-- bookkeeping.  This may not be intrinsically bad but it makes the share
-- files incompatible with the Tahoe-LAFS storage server on-disk state.  This
-- may not be intrinsically bad either but interop testing would be much
-- easier if the on-disk state were compatible.
uploadImmutableShares ::
  StorageIndex ->
  IO (Either [(ShareNum, StorageServer, B.ByteString)] Capability) ->
  IO Capability
uploadImmutableShares storageIndex uploads = do
  offsetRef <- newIORef (0 :: Offset)
  untilJust (uploadSome offsetRef)
  where
    uploadSome :: IORef Offset -> IO (Maybe Capability)
    uploadSome offsetRef = uploads >>= uploadNextChunks offsetRef storageIndex

    uploadNextChunks ::
      IORef Offset ->
      StorageIndex ->
      Either [(ShareNum, StorageServer, B.ByteString)] Capability ->
      IO (Maybe Capability)
    uploadNextChunks offsetRef storageIndex nextStep =
      case nextStep of
        Left chunks -> do
          offset <- readIORef offsetRef
          let (_, _, shareData) = head chunks
          writeIORef offsetRef (offset + (fromIntegral . B.length) shareData)

          mapM_ (uploadOneChunk offset storageIndex) chunks
          return Nothing

        Right cap ->
          return $ Just cap

    uploadOneChunk offset storageIndex (shareNum, server, shareData) =
      storageServerWrite server storageIndex shareNum offset shareData


-- | Encrypt and encode some application data to some ZFEC shares and upload
-- them to some servers.
store ::
  -- | The servers to consider using.
  [StorageServer] ->
  -- | The application data to operate on.
  Uploadable ->
  -- | The result of the attempt.
  IO UploadResult
store servers uploadable@(Uploadable key _ params _) =
  encryptAndEncode uploadable >>= upload servers key params

-- | Given some cleartext and some encoding parameters: encrypt and encode some
-- shares that can later be used to reconstruct the cleartext.
encryptAndEncode ::
  -- | The application data to encrypt and encode.
  Uploadable ->
  -- | An action to get an action that can be repeatedly evaluated to get
  -- share data.  As long as there is more share data, it evaluates to Left.
  -- When shares are done, it evaluates to Right.
  IO (IO (Either [B.ByteString] (AESKey128 -> Capability)))
encryptAndEncode (Uploadable key size params read) =
  chkEncrypt key read >>= makeChkEncoder params size

-- | Given some cleartext, some encoding parameters, and some servers:
-- encrypt, encode, and upload some shares that can later be used to
-- reconstruct the cleartext.
--
-- This replaces allmydata.immutable.upload.Uploader.upload.
upload ::
  -- | The servers to consider uploading shares to.
  [StorageServer] ->
  -- | The encryption key (to derive the storage index).
  AESKey128 ->
  -- | The encoding parameters (XXX only for happy, right?)
  Parameters ->
  -- | An action to repeatedly evaluate to get share data or the cap.
  (IO (Either [B.ByteString] (AESKey128 -> Capability))) ->
  -- | Describe the outcome of the upload.
  IO UploadResult
upload servers key params streams = do
  -- Decide where to put it
  existingShares <- locateAllShareholders storageIndex servers
  let targets = targetServers params existingShares

  -- Go
  cap <- uploadImmutableShares storageIndex (uploads targets streams)

  return $ UploadResult
    { uploadResultReadCap = cap
    , uploadResultExistingShares = fromIntegral $ length existingShares
    , uploadResultShareMap = targets
    }
  where
    storageIndex :: StorageIndex
    storageIndex = storageIndexHash key

    targetServers :: Parameters -> ShareMap -> ShareMap
    targetServers parameters existingShares = planSharePlacement parameters storageIndex existingShares servers

    -- Adapt a stream of share data to a stream of share data annotated with
    -- server placement decision.
    uploads
      :: ShareMap
      -- ^ The goals for share placement
      -> IO (Either [ByteString] (AESKey128 -> Capability))
      -- ^ The share data streams
      -> IO (Either [(ShareNum, StorageServer, B.ByteString)] Capability)
      -- ^ An action to take to make progress in completing the upload.  Left
      -- values describe some storage operation to perform.  Each storage
      -- operation comprises the next sequence of bytes to send to a given
      -- server for a given share number.  Right indicates the upload is
      -- complete and gives a capability for accessing the uploaded data.
    uploads goal streams = do
      encodeOutput <- streams
      case encodeOutput of
        Left shareDatav ->
          return . Left $ Map.foldrWithKey (makeUpload shareDatav) [] goal
        Right makeCap ->
          return . Right $ makeCap key
        where
          makeUpload ::
            [B.ByteString] ->
            ShareNum ->
            Set.Set StorageServer ->
            [(ShareNum, StorageServer, B.ByteString)] ->
            [(ShareNum, StorageServer, B.ByteString)]
          makeUpload shareDatav num servers soFar =
            [ ( num
              , server
              , shareDatav !! fromIntegral num
              )
            | server <- Set.elems servers
            ] ++ soFar

defaultParameters :: Parameters
defaultParameters = Parameters (128 * 1024) 10 7 3

-- The adjustment implemented in
-- allmydata.immutable.upload.BaseUploadable.get_all_encoding_parameters
adjustSegmentSize :: Parameters -> Size -> Parameters
adjustSegmentSize (Parameters segmentSize total happy required) dataSize =
  Parameters (effectiveSegmentSize dataSize) total happy required
  where
    effectiveSegmentSize =
      -- For small files, shrink the segment size to avoid wasting space.
      -- Also make the shrunk value a multiple of required shares or the
      -- encoding doesn't work.
      nextMultiple required . min segmentSize

-- Create an uploadable with the given key.
filesystemUploadable :: AESKey128 -> FilePath -> Parameters -> IO Uploadable
filesystemUploadable key path params = do
  fhandle <- openBinaryFile path ReadMode
  fsize <- hFileSize fhandle
  return $ Uploadable
    { uploadableKey = key
    , uploadableSize = fsize
    , uploadableParameters = adjustSegmentSize params fsize
    , uploadableReadCleartext = B.hGet fhandle . fromIntegral
    }

filesystemUploadableWithConvergence :: B.ByteString -> FilePath -> Parameters -> IO Uploadable
filesystemUploadableWithConvergence secret uploadablePath params = do
  -- Allow getConvergentKey to use lazy ByteString to read and hash the
  -- uploadable by letting the handle remain open past the end of this
  -- function.  lazy ByteString will close the handle.
  uploadableHandle <- openFile uploadablePath ReadMode
  hSetBinaryMode uploadableHandle True

  -- Annoyingly, adjust the segment size here so that the convergence secret
  -- is computed based on the adjusted value.  This is what Tahoe-LAFS does so
  -- it is necessary to arrive at the same converged key.  Whether this part
  -- of the construction is actually important aside from interop, I don't
  -- know.
  size <- hFileSize uploadableHandle
  content <- BL.hGetContents uploadableHandle
  let key = getConvergentKey secret (adjustSegmentSize params size) content

  print "Using convergence secret:"
  print secret
  print "Computed convergence key:"
  print . encodeBase32Unpadded . encode $ key
  filesystemUploadable key uploadablePath params

-- allmydata.immutable.upload.FileHandle._get_encryption_key_convergent
getConvergentKey :: B.ByteString -> Parameters -> BL.ByteString -> AESKey128
getConvergentKey secret params content =
  fromJust . buildKey $ convergenceEncryptionHashLazy secret params content


-- Create an uploadable with a random key.
filesystemUploadableRandomConvergence :: FilePath -> Parameters -> IO Uploadable
filesystemUploadableRandomConvergence path params = do
  key <- buildKeyIO :: IO AESKey128
  filesystemUploadable key path params


filesystemStorageServer :: FilePath -> IO StorageServer
filesystemStorageServer shareRoot = do
  createDirectoryIfMissing True shareRoot
  return $ StorageServer
    { storageServerID = pack shareRoot
    , storageServerWrite = writeShare
    , storageServerRead = undefined
    , storageServerGetBuckets = getBuckets
    }
    where
      writeShare :: StorageIndex -> ShareNum -> Offset -> B.ByteString -> IO ()
      writeShare = writeShareDataAt shareRoot

      writeShareDataAt :: FilePath -> StorageIndex -> ShareNum -> Offset -> ByteString -> IO ()
      writeShareDataAt shareRoot storageIndex shareNum offset xs = do
        createDirectoryIfMissing True (bucketPath shareRoot storageIndex)
        withBinaryFile (sharePath shareRoot storageIndex shareNum) ReadWriteMode $ \share_handle ->
          hSeek share_handle AbsoluteSeek offset >>
          hPut share_handle xs

      -- Get the path to the directory where shares for the given storage
      -- index should be written.
      bucketPath :: FilePath -> StorageIndex -> FilePath
      bucketPath root storageIndex = root </> bucketName
        where
          bucketName = "shares" </> shortPiece </> fullName
          fullName = unpack . Text.toLower . encodeBase32Unpadded $ storageIndex
          shortPiece = take 2 fullName

      -- Get the path to the file where data for the given share of the given
      -- storage index should be written.
      sharePath :: FilePath -> StorageIndex -> ShareNum -> FilePath
      sharePath root storageIndex shareNum =
        bucketPath root storageIndex </> show shareNum

      getBuckets :: StorageIndex -> IO (Set.Set ShareNum)
      getBuckets storageIndex =
        readShareFilenames `catch` doesNotExist
        where
          readShareFilenames =
            Set.fromList . map read <$> listDirectory (bucketPath shareRoot storageIndex)

          doesNotExist e =
            if isDoesNotExistError e
            then return Set.empty
            else ioError e

prettyFormatSharemap :: ShareMap -> Text
prettyFormatSharemap sharemap =
  intercalate "\n"
  [ Text.concat ["\t", showElem elem]
  | elem <- Map.toList sharemap
  ]
  where
    showElem :: (ShareNum, Set.Set StorageServer) -> Text
    showElem (shareNum, servers) =
      Text.concat $
      [ pack $ show shareNum
      , ":"
      ] ++ intersperse ", " (map storageServerID (Set.toList servers))
